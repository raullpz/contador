using Microsoft.VisualStudio.TestTools.UnitTesting;
using Contador;


namespace ContadorTest
{
    
    [TestClass]
    public class ContadorTest
    {

        private Conta contador = new Conta();

        [TestInitialize]
        public void Initialize()
        {
            //Arrange
            Conta contador = new Conta();
        }

      
        [TestMethod]
        public void IniciarContador()
        {

            //Act             
            contador.inicializar(50, 3, 4);

            //Assert 
            Assert.AreEqual(contador.Valor, 3);
            Assert.AreEqual(contador.Incremento, 4);
            Assert.AreEqual(contador.Limite, 50);
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void IniciarContadorExcepArgumentos()
        {

            //Act             
            contador.inicializar(null, 3, 4);

            Assert.Fail("Error ArgumentException");
        }

        [TestMethod]
        public void IniciarContadorValoresNegativos()
        {

            //Act 
            contador.inicializar(50,-1,-1);

            //Assert 
            Assert.AreEqual(contador.Valor, 0);
            Assert.AreEqual(contador.Incremento, 1);
            Assert.AreEqual(contador.Limite, 50);
        }

        [TestMethod]
        public void ValorIncremento0Y1SiNoEspecifica()
        {

            //Act 
            contador.inicializar(50);

            //Assert 
            Assert.AreEqual(contador.Valor, 0);
            Assert.AreEqual(contador.Incremento, 1);
            Assert.AreEqual(contador.Limite, 50);
        }


        [TestMethod]
        public void ContadorNoModificableUnaVezInicializado()
        {
 
            //Act
            contador.inicializar(50);
            contador.Valor = 34;
            contador.Incremento = 45;
            contador.Limite = 123;

            //Assert 
            Assert.AreEqual(contador.Valor, 0);
            Assert.AreEqual(contador.Incremento, 1);
            Assert.AreEqual(contador.Limite, 50);
        }

        [TestMethod]
        public void IncrementarContador()
        {

            //Act 
            contador.inicializar(3);

            for (int i = 0; i < 4; i++)
                contador.IncrementarContador();

            //Assert 
            Assert.AreEqual(contador.LimiteSuperado, true);
            Assert.AreEqual(contador.Valor, 0);
        }

        [TestMethod]
        public void SuperadoLimiteReiniciar()
        {

            //Act 
            contador.inicializar(3);

            for (int i = 0; i < 5; i++)
                contador.IncrementarContador();

            //Assert 
            Assert.AreEqual(contador.LimiteSuperado, true);
            Assert.AreEqual(contador.Valor, 1);
        }

        [TestMethod]
        public void ReiniciarValorInicialCualquierMomento()
        {

            //Act 
            contador.inicializar(3);

            for (int i = 0; i < 5; i++)
            {
                contador.IncrementarContador();

                if ((i % 2)==0)
                    contador.ReiniciarValorContador();
            }
                       
            //Assert 
            Assert.AreEqual(contador.LimiteSuperado, false);
            Assert.AreEqual(contador.Valor, 0);
        }

    }



}
