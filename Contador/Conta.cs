﻿using System;

namespace Contador
{
    public class Conta
    {

        private int valor;
        private int valorInicial;
        private int incremento;
        private int limite;
        private Boolean limiteSuperado;

        public int Valor
        {
            get { return valor; }
            set
            {

                if (valor == -1)
                    valor = value;
            }
        }

        public int ValorInicial
        {
            get { return valorInicial; }
            set { valorInicial = value; }
        }

        public int Incremento
        {
            get { return incremento; }
            set
            {
                if (incremento == -1)
                    incremento = value;
            }
        }

        public int Limite
        {
            get { return limite; }
            set
            {
                if (limite == -1)
                    limite = value;
            }
        }

        public Boolean LimiteSuperado
        {
            get { return limiteSuperado; }
            set { limiteSuperado = value; }
        }

        public Conta()
        {
            valor = -1;
            incremento = -1;
            limite = -1;
            limiteSuperado = false;
        }

        public void inicializar(object limite, int valor = 0, int incremento = 1)
        {

            if (limite == null)
            {

                throw new ArgumentException();

            }
            else
            {
                
                this.valor = (valor < 0) ? 0 : valor;
                this.valorInicial = this.valor;
                this.incremento = (incremento < 0) ? 1 : incremento;
                this.limite = Convert.ToInt32(limite);

            }

        }

        public void IncrementarContador()
        {
            valor++;

            if (ComprobarLimiteSuperado())
            {
                limiteSuperado = true;
                ReiniciarValorContador();
            }
        }

        private Boolean ComprobarLimiteSuperado()
        {
            if (valor > limite)
                return (true);
            else
                return (false);
        }

        public void ReiniciarValorContador()
        {

            valor = ValorInicial;

        }

    }

}
